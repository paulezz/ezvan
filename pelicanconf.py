#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Paul Ezvan'
SITENAME = 'Portail Ezvan'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'
LOCALE = 'fr_FR.utf8'

I18N_SUBSITES = {
    'en': {
        'SITENAME': 'Ezvan homepage',
    }
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

USE_FOLDER_AS_CATEGORY = True

# Blogroll
LINKS = (
    ("Roundcube", "https://www.ezvan.fr/roundcube"),
    ("Photos", "https://www.ezvan.fr/nextcloud"),
    ("Lecteur RSS", "https://www.ezvan.fr/rss/"),
    ("Agenda", "https://www.ezvan.fr/agenda"),
    ("Messagerie instantannée", "https://www.ezvan.fr/jappix/"),
)

# Social widget
SOCIAL = ()

# Plugins
PLUGIN_PATHS = [
    "../pelican-plugins",
    "../pelican-alias",
]
PLUGINS = [
    "i18n_subsites",
    "tipue_search",
    "neighbors",
    "pelican_alias",
]
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}
BOOTSTRAP_THEME = "united"
I18N_TEMPLATES_LANG = "en"

DISPLAY_ARTICLE_INFO_ON_INDEX = False
SHOW_ARTICLE_AUTHOR = True
SHOW_DATE_MODIFIED = True
SHOW_ARTICLE_CATEGORY = False
DISPLAY_AUTHORS_ON_SIDEBAR = True

# Archives
ARCHIVES_SAVE_AS = 'archives.html'
DISPLAY_ARCHIVE_ON_SIDEBAR = False

# Menu
MENUITEMS = ()

# Search
DIRECT_TEMPLATES = ('index', 'tags', 'categories', 'authors', 'archives', 'search')

THEME = "../pelican-themes/pelican-bootstrap3"

DEFAULT_PAGINATION = 5
SUMMARY_MAX_LENGTH = 200

READERS = {"html": None}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
