Title: Interpréteur BASIC
Date: 2022-01-02 22:00
Author: Dominique
Slug: interpreteur-basic
Lang: fr
Tags: Logiciels de Papa, SBASIC

Papa a profité de sa retraite pour porter sous Windows l’interpréteur *BASIC*, dénommé [**SBASIC**](https://sbasic.org/), qui était fourni avec les ordinateurs GOUPIL sous FLEX et MS/DOS. Si la plupart des BASIC sont devenus des compilateurs, [SBASIC](https://sbasic.org/) est resté un interpréteur très performant.

[**SBASIC**](https://sbasic.org/) profite des avantages d’un interpréteur : mode immédiat (exécution immédiate des instructions saisies au clavier) et mode programme, édition d’un programme durant son exécution, exécution d’instructions définies dynamiquement dans des chaines de caractères, auto-modification du programme, mise au point interactive, etc.

La compatibilité avec les programmes existants est conservée. Ainsi les fonctionnalités avancées du [SBASIC](https://sbasic.org/), comme les tableaux à dimensions multiples, les tableaux virtuels, les étiquettes, les appels de fonctions avec paramètres, les variables locales sont supportés. [SBASIC](https://sbasic.org/) sous Windows profite cependant des apports du 32 bits avec :  variables entières sur 32 bits, taille mémoire programme et données jusqu’à 2 Giga-octets, nombre d’éléments d’un tableau et nombre d’enregistrements dans un fichier jusqu’à 2^31, longueur chaines de caractères limitée par la mémoire disponible.

Sous Windows, [SBASIC](https://sbasic.org/) profite de nouvelles fonctionnalités : dimensionnement des fenêtres texte et graphique, copie d’un rectangle graphique en format BMP depuis et vers une chaine de caractères, sauvegarde et restauration d’une chaine de caractères dans un fichier, appel de fonctions dans une librairie dynamique (DLL), exécution en pas à pas.

[SBASIC](https://sbasic.org/) est disponible en deux versions : fenêtre Windows ou ligne de commande. Sur le site [sbasic.org](https://sbasic.org/) vous trouverez la documentation, [les exécutables](https://sbasic.org/telechargement/), des exemples, des jeux ainsi que des DLLs avec leur source.
