Title: Date d’expiration du certificat SSL d’un serveur XMPP
Date: 2021-03-31 09:00
Author: Paul
Tags: SSL, Jabber
Slug: certificat-ssl-expiration-xmpp
Lang: fr

Voici une astuce rapide pour vérifier la date d’expiration du certificat SSL d’un serveur XMPP. On utilise le client openssl en lançant `openssl s_client` pour initier une connexion TLS au serveur, pour cela on lui donne `-starttls xmpp` comme paramètre pour lui indiquer le protocole à utiliser.

On transmet le résultat via un pipe `|` à la commande `openssl x509` qui permet d’analyser le certificat fourni. [X.509](https://fr.wikipedia.org/wiki/X.509) est en effet la norme établissant le format de ces certificats. Le paramètre `-dates` indique spécifiquement d’afficher les dates de validité du certificat.

```shell
~% openssl s_client -connect mail.ezvan.fr:5222 -starttls xmpp -xmpphost ezvan.fr | openssl x509 -noout -dates
depth=2 O = Digital Signature Trust Co., CN = DST Root CA X3
verify return:1
depth=1 C = US, O = Let's Encrypt, CN = R3
verify return:1
depth=0 CN = ezvan.fr
verify return:1
notBefore=Mar 31 07:08:59 2021 GMT
notAfter=Jun 29 07:08:59 2021 GMT
```
