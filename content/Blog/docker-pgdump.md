Title: Utiliser pg_dump avec Docker
Date: 2022-01-02 19:00
Author: Paul
Slug: docker-pgdump
Lang: fr
Tags: PostgreSQL, Debian, Libre, conteneurs

Je suis en train de migrer Tiny Tiny RSS depuis AWS vers un serveur dédié. Pour ce faire j'essaie d'[effectuer un dump de la base de donnée](https://www.postgresql.org/docs/current/backup-dump.html) (stockée dans RDS) depuis une machine Debian Buster. Malheureusement `pg_dump` échoue car la version fournie par Debian est trop ancienne !
```shell
% pg_dump -h db2.ezvan.fr -U ttrss -W ttrss > ttrss-20220102.sql
Password: 
pg_dump: server version: 12.7; pg_dump version: 11.14 (Debian 11.14-0+deb10u1)
pg_dump: aborting because of server version mismatch
zsh: exit 1     pg_dump -h db2.ezvan.fr -U ttrss -W ttrss > ttrss-20220102.sql
```

### Docker à la rescousse

Je pourrais mettre à jour ma version de Debian pour obtenir un version plus récente de `pg_dump`, mais une telle migration serait chronophage. Une solution plus simple est d'utiliser Docker pour exécuter la version de `pg_dump` qui convient.

#### Installation de Docker

Attention sous Debian le nom du paquet Docker est `docker.io` !
```shell
sudo apt install docker.io
```
J'ajoute mon utilisateur au groupe docker. Je me reconnecte ensuite afin d'utiliser les nouvelles permissions.
```shell
sudo usermod -aG docker user
```
Je démarre le démon Docker.
```shell
sudo systemctl start docker
```
### Lancer pg_dump

Je lance `pg_dump` dans un conteneur Docker. J'utilise l'image `postgres` en spécifiant la version 12. Vous pouvez trouver la documentation de l'image sur [Docker Hub](https://hub.docker.com/_/postgres/).

```shell
docker run -i --rm postgres:12 pg_dump -h db2.ezvan.fr -U ttrss -W ttrss > ttrss-20220102.sql
```
J'utilise le paramètre `-i` pour pouvoir entrer un mot de passe si nécessaire. Attention, ne pas utiliser le paramètre `-t`, car cela peut faire échouer le dump car les caractères fin de ligne (EOL) sont interprétés et bloquent la commande.
