Title: Apple, ARM and open platforms
Date: 2020-11-19 21:00
Author: Paul
Tags: Libre, CPU, ARM, Apple
Slug: apple-arm-plateforme-ouverte
Lang: en

New Apple M1 based computers are getting reviewed by hardware websites. There is no doubt: it is a big slap in the face of their venerable x86 competitors in terms of performance and efficiency.

### A revolution

Is it the beginning of a new era in which our smartphones and personal computers would use the same [ARM](futur-arm.html) technology, which has powered smartphones since their beginnings?

Smartphones have taken over our world. They are everywhere, and required by more and more operations : validating banking transactions, payments, private and public transportation, etc.

It is a bigger revolution than the personal computer, which brought Internet and computing to mostly everybody.

### History repeats

However there is a major difference in how they get there to what happened with personal computers.

At their beginnings many companies, such as Amiga, Apple or Atari, were competing with different operating systems and processor architectures. Platforms were not compatible with each other, you couldn't run an Amstrad program on an Apple computer. Then the PC platform started to take off.

PC strength was that it is an open platform. An operating system and its software, running on a given PC compatible computer, would run on any other PC computer. Today the majority of personal computers are PC compatible, with the big exception of Apple hardware.

This enabled the success of Windows and then Linux. Other less common PC systems are able to run on wide range of PC computers, such as various Unix and BSDs.

### A closed world...

ARM is a different world in which there is no platform definition. It is difficult if not impossible to run an Iphone's system on an Android phone, or vice versa. It is even difficult to run a Samsung phone system on another brand's phone, even if both are Android based.

This makes it very challenging to build alternative smartphone operating systems. Projects such as [LineageOS](https://lineageos.org/) are trying to provide alternatives to the manufacturer ones, but they require a large porting work for each device.

As well initiatives such as [postmarketOS](https://postmarketos.org/), which allows running a real Linux system on a smartphone exist, but the porting challenge is so huge that only a handful of devices are supported.

### For a locked future ?

Apple went down the same road with its Macbooks, providing hardware and software in an integrated platform. Installing something else than MacOS on a recent Macbook is possible, but requires a lot of effort.

Iphones are completely locked. You can't install any other system on them. There is even no way you can install apps which are not approved by Apple on an Iphone. Will Apple take advantage of the ARM transition on its Macbooks to impose the same restrictions?

If it does, will we trade the freedom of running the software we want with much better performance and efficiency? Will the fully locked down smartphone model overtake the open PC platform one?

Will we allow only fully locked systems become essential tools of our lives?
