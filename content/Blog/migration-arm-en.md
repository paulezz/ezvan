Title: ARM is the future
Date: 2020-11-19 20:00
Author: Paul
Tags: Services, CPU, ARM
Slug: futur-arm
Lang: en

ARM processors are everywhere! There are the heart of the smartphones, TVs, Wifi routers, 5G networks, etc.

However in our personal computers, x86 based processor still largely prevail, even if x86 was invented more than fifty years ago. It is also the case in the datacenter world, nicely called cloud nowadays.

Times are changing. Major actors such as Amazon and Apple are embracing ARM to put their dependency on Intel to an end. As ARM only defines the architecture of the CPU, many companies are competing in building ARM powered chips. That allows breaking Intel's near monopoly on personal and datacenter computers.

Are we at the dawn of a new era, where ARM would fully replace the good old x86?

An another precursor has joined the game, as Ezvan.fr was migrated to ARM recently. Faster and cheaper, is there more to ask for?
