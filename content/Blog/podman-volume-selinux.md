Title: Podman, volume et SELinux
Date: 2021-04-26 20:00
Author: Paul
Tags: Libre, Linux, Podman, SELinux
Slug: podman-volume-selinux
Lang: fr

En ce moment j’expérimente avec [Rust](https://www.rust-lang.org/), le langage de programmation à la mode. Ayant compilé mon programme expérimental sur mon système [Fedora](https://getfedora.org/), je l’envoie à un ami. Quelle déception quand il me dit que mon programme répond mal : 

```shell
/lib/x86_64-linux-gnu/libc.so.6: version `GLIBC_2.32' not `found
```

Rust compile avec la version de la librairie [glibc](https://www.gnu.org/software/libc/) de mon système, qui est trop récente et pas compatible avec [Ubuntu](https://ubuntu.com/). Je dois donc compiler avec un système plus ancien. Pas de panique les conteneurs sont parfaits pour ça !

Comme je travaille avec [Fedora](https://getfedora.org/), j’utilise [Podman](https://podman.io/) qui est un équivalent à [Docker](https://www.docker.com/) pour créer mon conteneur.

Je tente ainsi, mais c’est l’échec :

```shell
% podman run --rm -v "$PWD":/usr/src/ -w /usr/src/ rust cargo build --release 
error: could not find `Cargo.toml` in `/usr/src` or any parent directory
zsh: exit 101   podman run --rm -v "$PWD":/usr/src/ -w /usr/src/ rust cargo build --release
```

D’où vient cette erreur ? Je tente de lire le répertoire depuis le conteneur :

```shell
% podman run --rm -v "$PWD":/usr/src/ rust ls /usr/src
ls: cannot open directory '/usr/src': Permission denied
zsh: exit 2     podman run --rm -v "$PWD":/usr/src/ rust ls /usr/src
```

Je ne peux pas lire le répertoire partagé dans un volume par Podman.

Mais pourquoi donc ? J’utilise Fedora, et donc [SELinux](https://selinuxproject.org/) est activé par défaut. SELinux est un module de sécurité qui permet de définir des politiques de contrôle d’accès aux éléments du système. Il peut, entre autre, restreindre les accès au système du fichier en fonction du contexte. Il faut donc donner permission au conteneur d’accéder au volume !

Pour ce faire il suffit d’ajouter une option **:z** ou **:Z** à la configuration du volume. L’option **z** permet à plusieurs conteneurs de partager le volume. L’option **Z** ne permet l’accès qu’à un seul conteneur. Tous les détails sont donnés dans `man podman-run` !

Je n’ai pas besoin de partager le volume, j’opte donc pour **:Z**.

Voici le résultat:

```shell
% podman run --rm -v "$PWD":/usr/src/:Z -w /usr/src/ rust cargo build --release
    Updating crates.io index
 Downloading crates ...
    Finished release [optimized] target(s) in 24.60s
```
Cette fois cela fonctionne, j’ai bien compilé mon programme à l’aide du [conteneur rust](https://hub.docker.com/_/rust/) !
