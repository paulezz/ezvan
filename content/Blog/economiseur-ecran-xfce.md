Title: Firefox, économiseur d'écran et Xfce
Date: 2023-01-08 18:00
Author: Paul
Tags: Linux, Libre, Firefox, Xfce
Slug: economiseur-ecran-xfce
Status: published
Lang: fr

*Comment un bogue énervant m'a conduit à contribuer à Firefox.*

### Contexte

J'ai la fâcheuse manie de changer l'environnement de bureau de mon ordinateur tous les 6 mois à peu près. À cette occasion, j'utilise régulièrement [Xfce](https://xfce.org/) dont la simplicité et légèreté me plaisent.

Lors de mon dernier essai, je remarque un comportement fâcheux : l'écran se met en veille lorsque je visionne une vidéo Youtube avec Firefox. Encore une fâcheuse manie !

### On creuse

Je profite de l'occasion pour creuser le problème et voir si je peux le résoudre. Après quelques recherches, je trouve un [rapport de bogue](https://gitlab.xfce.org/xfce/xfce4-power-manager/-/issues/42) similaire à mon problème, lié au composant *xfce4-power-manager*.

Le mainteneur de projet suggère que Firefox n'appelle pas la bonne interface *D-Bus* lorsqu'il veut inhiber la mise en veille de l'écran. *D-Bus* est un démon qui permet de faire communiquer différents programmes entre eux, via des interfaces pré-définies. Le problème n'apparait pas avec Chromium, qui doit certainement faire quelque chose de différent !

### Hacker Firefox

Je plonge donc dans les entrailles de Firefox. Je trouve le fichier qui s'occupe de cette fonctionnalité : [WakeLockListener.cpp](https://hg.mozilla.org/mozilla-central/file/tip/widget/gtk/WakeLockListener.cpp). Je compare avec le code qui remplit la même fonction chez Chromium pour avoir une meilleure idée de ce qui peut mal fonctionner : [power_save_blocker_x11.cc](https://chromium.googlesource.com/chromium/src/+/a8498df479418cae7b379aca518d04d0648f3a68/device/power_save_blocker/power_save_blocker_x11.cc).

Je remarque que Chromium appelle toutes les fonctions D-Bus connues comme désactivant la mise en veille en même temps. Par contre, Firefox les appelle dans l'ordre, et s'arrête dès qu'un appel est concluant.

De plus, Firefox n'appelle pas l'interface utilisée par le composant *xfce4-power-manager*, celle-ci étant : **org.freedesktop.PowerManagement.Inhibit**.

J'essaie donc de modifier le comportement de Firefox pour ajouter cet appel. Comme changer le comportement de Firefox pour suivre celui de Chromium me semble un trop grand changement, j'essaie plus simplement d'ajouter le bon appel.

J'arrive à faire fonctionner et tester la modification sur mon ordinateur. Je soumets un [rapport de bogue](https://bugzilla.mozilla.org/show_bug.cgi?id=1733796) à Firefox ainsi qu'une *pull request* avec [ma modification](https://phabricator.services.mozilla.com/D127363).

Un des mainteneurs de Firefox demande quelques modifications, que j'effectue. Il valide ensuite mon changement et l'intègre dans la branche principale. Hourra !

### Déception

J'attends que la version 95 de Firefox soit publiée, qui contiendra alors mon changement. Et là, patatras ! Il manque quelque chose, mon écran s'éteint toujours lorsque je regarde une vidéo. J'ai raté quelque chose lors de mes tests !

Je me replonge dans le problème. Il me semble qu'il manque quelque chose dans *xfce4-power-manager*. En effet, le démon reçoit bien la demande d'inhibition de l'écran. Mais il manque l'appel pour inhiber l'écran dans la fonction qui gère la demande !

Je crée donc une nouvelle *pull request*, cette fois pour *xfce4-power-manager*, afin d'[ajouter cet appel manquant](https://gitlab.xfce.org/xfce/xfce4-power-manager/-/merge_requests/22).

### Tout est bien qui finit bien

Finalement tout fonctionne chez moi ! Mais encore faut-il attendre que ma *pull request* soit acceptée et une nouvelle version de Xfce soit publiée avec le correctif.

Après quelques modifications, la pull request est acceptée. Elle est intégrée dans la version 4.18 de Xfce, qui sortira 11 mois plus tard.

Tout semble fonctionner dorénavant, et il ne semble pas avoir cassé quelque chose. Quelqu'un se serait plaint sinon !
